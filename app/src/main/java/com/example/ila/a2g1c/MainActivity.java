package com.example.ila.a2g1c;

import android.app.Activity;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    NfcAdapter adapter;
    Parcelable mytag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        adapter = NfcAdapter.getDefaultAdapter(this);

    }

    @Override
    protected void onNewIntent(Intent intent){

        if(NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())){
            mytag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);  // get the detected tag
            Parcelable[] msgs =
                    intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefRecord firstRecord = ((NdefMessage)msgs[0]).getRecords()[0];
            byte[] payload = firstRecord.getPayload();
            int payloadLength = payload.length;
            int langLength = payload[0];
            int textLength = payloadLength - langLength - 1;
            byte[] text = new byte[textLength];
           // System.arraycopy(payload, 1+langLength, text, 0, textLength);
            //Toast.makeText(this, "OK "+new String(text), Toast.LENGTH_LONG).show();
             TextView screenText = findViewById(R.id.mainSceenText);
            screenText.setText(text.toString()+payload.toString());
        }
    }



    protected void getCupInfo(byte[] pay) {

    }

}


